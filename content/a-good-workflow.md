Title: Build an optimized workflow!
Date: 2022-01-28 10:20

There are a few things that set apart true professionals from the rest in 
their trade. Among these things are a refined workflow and a mastery of one's 
tools.

No matter the industry, this philosphy holds true: A great carpenter will 
know the purpose and location of every tool in their shop. And you can use 
the same idea for every single field, doesn't matter who you're, knowing the 
tools will guide you to the excelence.

A lot of times, we can expect programs to ship with what are commonly 
referred to as "sane defaults". This means that for a new user, 
there shouldn't be very much to configure in order to get up and running. 
The settings are preconfigured in a way that makes sense for the average use-case. 
This is a tragedy, because if we fully harnessed the power of the software we 
already use, our productivity would greatly increase.

Everytime I find myself thinking "there has got to be a better way to do this...", 
I actually take the time to find and master that better way. And if it 
doesn't exist, I do my best to make it. Make the things happen.

But why spend hours writing config files when you could just be writing code?

There are lots of benefits of building your own workflow, from increasing 
raw productivity to passively gaining knowledge by exposure to the granular 
settings offered by most tools and programs.

It wasn't until I realized that the only way to be better is mastery the 
tools and of course follow a path to ensure that everything is aligned in 
the best way as possible.

That moment was when my productivity started showing up.

Dont waste your time!

Build an Optimized Workflow!
