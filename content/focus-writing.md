Title: Simple Steps for Focus on Writing
Date: 2018-06-09 10:20

So, this is a rapid journal essay, hand written and extracted from an Instagram response to one of my friends. Normally these type of entries contain a bunch of steps, rituals or whatever for achieve the maximun focus. These are my more common used steps.

- Put your editor or main app in full screen mode. No clocks, no battery stats, no menus; just you and your app. The most important thing here is a "distraction less" window.
- Choose a good ambient music, normally it's based on my mood from Instrumental or Symphonic Rock to Opera ending with Classical music.
- Hide your phone from your visibility range. If you have an iPhone it have a "don't disturb mode" in your settings or in your control center. Go away from the notifications of your family group.
- Find a place without noise or just use a good headphones, if it have noise cancelling it would be perfect.
- Pick a cozy place, usually in the corner of my room/office 'cause it make me feel good.

## Extra Bonus

My workflow when I'm writing is:

- Do a mental map about the stuff that I want to write.
- Move the ideas to a blank paper.
- Transcribe and improve from the paper to a text processor.
- Create the first draft and read it almost three times.
- Share it with your closest friend.
- If everything is ok, publish it otherwise repeat.

Some useful "distraction less" text processor apps:

- iA Writer > [https://ia.net/writer](https://ia.net/writer)
- Left > [https://hundredrabbits.itch.io/left](https://hundredrabbits.itch.io/left)
- Moeditor > [https://moeditor.js.org/](https://moeditor.js.org/)

See you soon, Oscar :)
