Title: A Monochrome Day
Date: 2018-04-13 10:20

Some days ago while inspecting database queries in a RoR project, I asked for advice to one of my coworkers cause the things started going crazy, after some time doing pair debugging, he said something like this:

`
"I can't understand it, because in my editor I see full colored syntax, so I don't know what is that, it's an object? a helper? a model?"
`

I know a few people whose use these features for understand each piece of their code, that's why they believe that using an IDE is much better than an editor or at least use a fully covered syntax theme.

| ![Neon theme; by MattDMo.]({attach}images/neon-theme.png "Neon Theme") |
|:--:|
| *Sublime Text with Neon Theme* |

These days I've switched in my editor from a **rainbow** to a **monochromatic** syntax theme, because I started feeling kinda disappointed, when change from one to another programming language, like *Ruby* to *JavaScript* for example.

There's a lot of themes around the web who's are built with a less expressive syntax in mind:

- [Duotone *by* Simurai](https://github.com/simurai/duotone-syntax)
- [Apex *by* Apex](https://github.com/apex/apex-ui/)
- [Ayu *by* Dempfi](https://github.com/dempfi/ayu)
- [Chestnut *by* Biletskyy](https://github.com/biletskyy/chestnut-light-atom-syntax)
- [Mono *by* Chrstphrknwtn](https://github.com/chrstphrknwtn/mono-ui)

Currently I'm using **VS Code** as my main editor so I use a theme named [White](https://github.com/arthurwhite/white-theme-vscode) *brought to you by* [Arthur White](https://github.com/arthurwhite), who recommend use a theme switcher for day/night development (very helpful)

| ![Visual Studio Code; White theme]({attach}images/mono-white.png "White Version") |
|:--:|
| *Visual Studio Code White Theme* |

----

| ![Visual Studio Code; White theme / Dark variant]({attach}images/mono-dark.png "Dark Version") |
|:--:|
| *Visual Studio Code White Theme* |

And also I added two small tweaks for a better experience:

- Replace inverted tabs color
- Add active tabs border

```
"workbench.colorCustomizations": {
  "[White Night]": {
      "tab.activeBackground": "#000",
      "tab.activeForeground": "#fff",
      "tab.activeBorder": "#fff"
  },
  "[White]": {
      "tab.activeBackground": "#fff",
      "tab.activeForeground": "#000",
      "tab.activeBorder": "#000"
  },
}
```

After some days using it, my eyes started feeling less tired also noticed an improvement in my concentration and the capacity of understand things around the project.

▲ I really like the new minimalistic view in my editor + zen mode.

See you soon, Oscar :)
