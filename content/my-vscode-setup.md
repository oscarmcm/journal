Title: Que hay en mi editor - Edicion 2020
Date: 2020-07-06 10:20

Son las 10 de la noche con 37 minutos (23:37) del 5 de Julio del 2020, mi novia se ha ido a dormir, el tema del coronavirus ronda mi cabeza por enésima vez, y al mismo tiempo pienso en hacer un podcast (¿me subo al tren del mame?) sobre minimalismo con mi mejor amigo, y bueno para no hacer mas largo esto, por que no darle inicio, con las cosas que uso día a día, empezando con mi editor de texto.

## ¿Qué editor de texto uso?

Creo que en todos estos años he pasado por cualquier cantidad de editores de texto, desde los mas esotéricos como [SpaceMacs](https://www.spacemacs.org/) hasta los mas populares como [Sublime Text](https://www.sublimetext.com/) (aunque ya esta mas muerto que [Atom](https://atom.io/)), pero en lineas generales me he movido a usar [Visual Studio Code](https://code.visualstudio.com/) desde hace como 3 años creo, y a pesar de que no soy muy fan, le he encontrado el gusto.

## ¿Como se ve?

No creo que hayas leído mi entrada [A monochrome day](https://journal.oscarmcm.com/entry/monochrome-day/) pero bien, aun sigo sobre la misma linea/idea, así que sí, mi editor es todo blanco o negro, depende de la cantidad de luz que tenga mi lugar de trabajo, aunque hay veces que se pone mas colorido :wink:.

| ![editor-white.png]({attach}images/editor-white.png) |
|:--:|
| *Visual Studio Code Monochrome White* |

## ¿Que si y que no?

Una de las cosas que nunca me ha gustado de cualquier editor, es toda ese montón de iconos extras (basura visual) que se puede activar o ejecutar desde un menu contextual, así que lo primero que hice fue ocultar todo aquello que no uso, y si te preguntas por que no uso VIM o algo por el estilo, es por que no soy muy fanático de los buffers pero si de los tabs :wink:, volviendo al tema, aquí esta mi configuración de las cosas que cambié:

```json
	"editor.renderWhitespace": "selection",
	"editor.glyphMargin": false,
	"editor.minimap.enabled": false,
	"editor.renderIndentGuides": false,
	"editor.codeLens": false,
	"editor.renderLineHighlight": "gutter",
	"editor.overviewRulerBorder": false,
	"editor.hideCursorInOverviewRuler": true,
	"workbench.activityBar.visible": false,
	"explorer.openEditors.visible": 0,
	"breadcrumbs.enabled": false,
	"zenMode.fullScreen": false,
	"window.titleBarStyle": "native",
	"window.nativeTabs": false,
	"window.title": " ",
	"workbench.fontAliasing": "antialiased",
	"workbench.statusBar.visible": false,
	"workbench.activityBar.visible": false,
	"workbench.editor.showIcons": false,
	"workbench.startupEditor": "newUntitledFile",
	"workbench.editor.tabCloseButton": "off",
	"workbench.tree.renderIndentGuides": "none",
	"workbench.iconTheme": "vs-minimal",
```

## Extensiones y temas

Sí, a como esperas, el tema que uso se llama [Monochrome](https://marketplace.visualstudio.com/items?itemName=anotherglitchinthematrix.monochrome) junto con los _VS Code Minimal Icons_.

### Temas

- [MNO](https://github.com/u29dc/set-vscode)
- [MIN](https://marketplace.visualstudio.com/items?itemName=miguelsolorio.min-theme)
- [WHITE](https://github.com/arthurwhite/white-theme-vscode)
- [DOBRI](https://marketplace.visualstudio.com/items?itemName=sldobri.bunker)

### Extensiones

- [Disable Ligatures.](https://marketplace.visualstudio.com/items?itemName=CoenraadS.disableligatures)
- [Bracket Pair Colorizer.](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer)
- [TODO Highlight.](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight)
- [Keen neutral icon theme.](https://marketplace.visualstudio.com/items?itemName=keenethics.keen-neutral-icon-theme)


## Minimalismo

A como podrás imaginar, muchas cosas no se pueden lograr utilizando la configuración normal de Visual Studio Code, es por ello que hago uso de dos extensiones muy populares, para alcanzar el máximo nivel de minimalismo posible:

- [Customize UI](https://marketplace.visualstudio.com/items?itemName=iocave.customize-ui)
- [Monkey Patch](https://marketplace.visualstudio.com/items?itemName=iocave.monkey-patch)

Y aparte de ello, tengo un poco de extra CSS para eliminar esos iconos que no se van del explorador de archivos, así que agrega esto a tu archivo de configuración:

```json
"customizeUI.titleBar": "inline",
"customizeUI.stylesheet": {
	".sidebar .actions-container": "display: none !important;",
	".title-label": "display: none !important;",
	".monaco-workbench .part.editor > .content .editor-group-container > .title .editor-actions": "display: none !important;",
},
```

Casi de ultimo, y la mas reciente de mis modificaciones, si alguna vez utilizaste Atom, te habrá encantado el _command palette dialog_, aunque el de VS Code hace sentido, sigo sintiendo nostalgia por el, así que un día de estos decidí ver si era posible, y este es el resultado:

| ![editor-white-modal.png]({attach}images/editor-white-modal.png) |
|:--:|
| *Visual Studio Code Monochrome White With Modal* |

Si te gusta solo tienes que agregar lo siguiente a tu configuración de `"customizeUI.stylesheet"`:

```json
".quick-input-widget": "top: 25% !important; box-shadow: rgba(0, 0, 0, 0.75) -1px 1px 5px 900px !important;"
```

## Pequeñas cosas

- `"files.trimTrailingWhitespace": true`.
- `"editor.cursorStyle": "underline"`.
- `"workbench.sideBar.location": "right"`: _guilty pleasure_.
- `"editor.emptySelectionClipboard": false`: Por si copias algo sin tener nada seleccionado en el editor.
- `"editor.formatOnPaste": true`.
- `"editor.fontFamily": "iosevka"`: De mis fuentes favoritas para programar, con un buen soporte para font ligatures.

Y con eso cerramos esta entrada en mi journal, espero que te sirva de algo y puedas llevar tu editor al siguiente nivel.

| ![editor-dark.png]({attach}images/editor-dark.png) |
|:--:|
| *Visual Studio Code Monochrome Dark* |

Hasta pronto, Oscar :)

-------

La mayoría del contenido acá presente, está basado en las siguientes referencias.

- [@calebporzio - My VS Code Setup](https://calebporzio.com/my-vs-code-setup-2/)
- [@marciobarrios - Minimal VS Code Interface](https://medium.com/@marciobarrios/minimal-user-interface-for-visual-studio-code-2ab849eb6d8e)
- [@gaearon - Subliminal](https://github.com/gaearon/subliminal)
