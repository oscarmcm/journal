Title: Creativity Is Not A Constrained Event
Date: 2023-08-30 10:20

How often do you feel that the lack of creativity is stopping you from getting
something done? I've been thinking a lot about this essay and never had the
motivation to finally get it done (well, until ¿today?) - What a paradox.

Creativity, in its essence, is a boundless wellspring that transcends
limitations and transforms them into stepping stones toward unparalleled innovation.
While Constraints, far from inhibiting creativity, serve as catalysts that
stimulate unconventional thinking and problem-solving.

After finishing watching [BREAK IT ALL: The History of Rock in Latin America](https://www.netflix.com/title/81006953)
I was surprised how creative all of those musicians were, thriving from one
genre or style to another, mixing up things, and so on. I questioned inside of me
how they were able to achieve that? Looking back on my life, I remember that
on my childhood I wanted to be a musician, but there was a moment that
probably marked a no return point on my goal.

When I was learning to play guitar, I often had that "bad thumb" position issue:

!["-"](https://images.squarespace-cdn.com/content/v1/56c240a0d51cd440f4c3f6ca/1486566318227-VUJRP7LJMRZ3QBA4RF94/foto-13-pulgar-arriba-apaga-sexta-cuerda.jpg "-")

Most purist teachers will do as much as they can to get that thumb in the right
position, because well... you must learn the things in the right way:

!["-"](https://i0.wp.com/guitarra-acustica.com/wp-content/uploads/2018/02/IMG_3166.jpg "-")

What happened? Easy, I got frustated, and quit my lessons. Yes, that was an
inmature decision, but hey, I just only wanted to play a guitar.

When you see all of those genious in the documentary film, the only thing I
see is that their creativity was not constrained into something, it doesn't matter
how prolific they were using an instrument, what does matter is how they get the
things done, and from where that creativity was derived.

> https://youtu.be/EUbUn9FnrME?si=2lPKy5pMyv-v1Rir&t=132
> Rick Rubin: The 60 Minutes Interview

The intertwining of technology and creativity further exemplifies how breaking
free from traditional boundaries can lead to unparalleled achievements, for
example when I was working on the [SDK](https://s3.amazonaws.com/hyfm/static/js/crypto.js)
for [HelpYouFind.Me](https://helpyoufind.me) I always had this question on my mind:

> How I can abstract the [WebCrypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API)
to suit our needs.

I believe that this is the reason why I got stuck into programming, I can do
the things the way I want, and while there's some rules out there, those are
not written in gold. Some problems in CS might give you the posibility to be
a creative mind, this will help to find a good number of possible solutions - aside
of that, this uncontrolled creative process will generate chaos - that later on
the expertice you had on the topic will guide to pick the most adequate solution.

However, it is essential to acknowledge that the unconstrained nature of
creativity does not negate the role of structure and discipline in its pursuit.
While creative thoughts may seem to materialize randomly, they often stem
from a foundation of knowledge, practice, and deliberate effort.

Creativity is not a constrained event; it is a force that thrives on challenges,
limitations, and deviations from the norm. It is the marriage of expertise,
audacity, and a willingness to embrace limitations that gives rise to boundless
potential to solve or do something.
