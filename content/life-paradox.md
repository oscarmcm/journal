Title: The Life Paradox
Date: 2018-02-10

> It's Hard to forego efficiency, we live in a world of tradeoffs.

> Spending some time optimizing a certain function takes your focus and time away from optimizing another.

> We all want everything to work perfectly, look beautiful, be intuitive and perform really well.

> This is true for your life as it is true for engineering.

> You want inbox zero, a healthy mind and body, up to date software development skills, design ability, a happy team, a happy family, an impressive bookshelf, a great social life...

> The problem is: You can’t spend all of your time and resources optimizing everything at once. Any extra minute you dedicate to an activity takes it away from something else.


Extract from: [It's Hard to Forego Efficiency](https://rauchg.com/2017/its-hard-to-forego-efficiency) by Guillermo Rauch - All credits to the author.
